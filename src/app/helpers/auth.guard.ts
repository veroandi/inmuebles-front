import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, Router} from '@angular/router';
import { LoginService } from '../modules/autenticacion/services/login.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(private _loginService: LoginService,
    private _router: Router,
  ) { }

  canActivate(): Observable<boolean | UrlTree> {

    return this._loginService.isLoggedIn$.pipe(
      map(loggedIn => loggedIn ? true : this._router.parseUrl('/login'))
    )
  }
}





