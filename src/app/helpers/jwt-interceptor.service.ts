import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http'
import { LoginService } from '../modules/autenticacion/services/login.service';


@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService implements HttpInterceptor {

  constructor(private login: LoginService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    if (req.url.includes('/login')) {
      return next.handle(req);
    }
    let tokenizedReq = req.clone({
      setHeaders: {
        Authorization: `${this.login.getTokenAutenticado()}`
      }
    });

    return next.handle(tokenizedReq);
  }
}
