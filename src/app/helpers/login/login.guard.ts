import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from 'src/app/modules/autenticacion/services/login.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor( private _loginService:LoginService,
               private router:Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree>  {
    
        //si el usuario está autenticado no debe entrar al componente login
        return this._loginService.isLoggedOut$.pipe(
          map(loggedOut=>  
              loggedOut?true:this.router.parseUrl('/')
            )
        )
  }
  
}
