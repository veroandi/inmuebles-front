import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InmueblesMensajesFormularioComponent } from './inmuebles-mensajes-formulario.component';

describe('InmueblesMensajesFormularioComponent', () => {
  let component: InmueblesMensajesFormularioComponent;
  let fixture: ComponentFixture<InmueblesMensajesFormularioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InmueblesMensajesFormularioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InmueblesMensajesFormularioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
