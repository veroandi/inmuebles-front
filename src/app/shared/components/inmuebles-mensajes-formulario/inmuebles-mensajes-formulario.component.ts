import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'inmuebles-mensajes-formulario',
  templateUrl: './inmuebles-mensajes-formulario.component.html',
  styleUrls: [ './inmuebles-mensajes-formulario.component.scss']
})
export class InmueblesMensajesFormularioComponent implements OnInit {

  @Input() validaciones: any[];
  @Input() control: string;
  @Input() formulario: FormGroup;


  public validacionControl: boolean;

  constructor() { }

  ngOnInit() {
  }
  get controlNoValido() {
    this.validacionControl = this.formulario.get(this.control).invalid && this.formulario.get(this.control).touched;
    return this.validacionControl;
  }

}
