import { Component, Input, forwardRef, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { SelectList } from '../../models/select-list';

@Component({
  selector: 'inmuebles-selector-reactivo',
  templateUrl: './inmuebles-selector-reactivo.component.html',
  styleUrls: ['./inmuebles-selector-reactivo.component.scss'],
  providers: [
    {
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => InmueblesSelectorReactivoComponent),
        multi: true
    }
  ]
})
export class InmueblesSelectorReactivoComponent implements ControlValueAccessor, OnChanges {
  @Input() items: SelectList[];
  @Input() readonly: boolean;
  @Input() searchable = true;
  @Input() clearable = true;
  @Input() multiple: boolean;
  @Input() defaultOption: boolean;
  @Input() placeholder = 'Seleccione...';
  @Input() name: string;
  @Input() id: string;
  @Input() defaultCode: string;
  @Input() bindLabel = 'nombre';
  @Input() bindValue = '';
  @Input() notFoundText = 'No hay registros';

  // tslint:disable-next-line: no-output-native
  @Output() change: EventEmitter<any> = new EventEmitter();

  public defaultValue: any;
  public value: any;

  private onChangeCallback: (_: any) => void;
  private onTouchCallback: (_: any) => void;
  title: string = this.placeholder;

  ngOnChanges(changes: SimpleChanges): void {

  }

  changeSelector(val: any) {
    this.onChangeCallback(val);
    this.onTouchCallback(val);
    this.change.emit(val);
  }

  writeValue(value: any): void {
    this.value = value;
    if (value !== '') {
      this.defaultValue = value;
    }
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.readonly = isDisabled;
  }
}
