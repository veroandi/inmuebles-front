import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InmueblesSelectorReactivoComponent } from './inmuebles-selector-reactivo.component';

describe('InmueblesSelectorReactivoComponent', () => {
  let component: InmueblesSelectorReactivoComponent;
  let fixture: ComponentFixture<InmueblesSelectorReactivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InmueblesSelectorReactivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InmueblesSelectorReactivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
