export enum patronValidacion {
  soloNumeros = '^[0-9]+$',
  alfanumerico = '^[a-zA-Z0-9]*$',
  soloTexto = '^[a-zA-ZñÑäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙ ]+$',
  textoNumGuion = '^[a-zA-Z0-9_]+$',
  textoCaracteres = '^[ a-zA-ZñÑäÄëËïÏöÖüÜáéíóúáéíóúÁÉÍÓÚÂÊÎÔÛâêîôûàèìòùÀÈÌÒÙ)(.-]+$',
  textoGuionUnderline = '^[a-zA-ZñÑ_-]+$',
  email = '^[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}',
  numeroDecimal = '^[0-9]+(\.[0-9]{1,3})?$',
  alfanumericoEspacio = '^[a-zA-Z0-9 ]+$',
  url = '^(https?:\/\/)*[a-z0-9-]+(\.[a-z0-9-]+)+(\/[a-z0-9-]+)*\/?$',
  fecha = '^([0-9][0-9])?[0-9][0-9]-(0[0-9]||1[0-2])-([0-2][0-9]||3[0-1])$'
}

export enum patronMensaje {
  soloNumeros = 'Solo se acepta números',
  alfanumerico = 'No se aceptan caracteres especiales',
  requerido = 'Este campo es obligatorio',
  soloTexto = 'No se aceptan números o caracteres especiales a excepción de letras acentuadas',
  textoNumGuion = 'No se aceptan espacios en blanco o caracteres especiales a excepción de guión bajo.',
  textoCaracteres = 'No se aceptan números o caracteres especiales a excepción de paréntesis, guión, punto o letras acentuadas',
  textoGuionUnderline = 'No se aceptan números, espacios en blanco o caracteres especiales a excepción de guión medio y guión bajo.',
  numeroDecimal = 'Solo se aceptan números; con máximo 3 decimales',
  email = 'Ingrese un formato de correo válido',
  fecha = 'Ingrese un formato de fecha válido',
  alfanumericoEspacio = 'No se aceptan caracteres especiales.',
  url = 'Ingrese una URL válida',
}

