import { TestBed } from '@angular/core/testing';

import { MensajesModalService } from './mensajes-modal.service';

describe('MensajesModalService', () => {
  let service: MensajesModalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MensajesModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
