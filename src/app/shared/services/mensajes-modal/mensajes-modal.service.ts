import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { BehaviorSubject, from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MensajesModalService {

  constructor() {
   }

  abrirMensajeModal({
    tipo,
    titulo,
    texto = '',
    btntext = 'Aceptar',
    btnCerrar = 'Cancelar',
    showConfirmButton = true,
    showCancelButton = true,
    textHtml = ''
  }) {

    const modalBehavior = new BehaviorSubject({});
    const $modalBehavior = modalBehavior.asObservable();

    switch ( tipo ) {
      case 'confirm': {
        Swal.fire({
          title: titulo,
          text: texto,
          icon: 'success',
          html: textHtml,
          showCancelButton: true,
          allowOutsideClick: false,
          allowEscapeKey: false,
          confirmButtonColor: '#E81F76',
          cancelButtonColor: '#ffffff',
          cancelButtonText: 'Cancelar',
          confirmButtonText: btntext,
          reverseButtons: true,
          customClass: {
            popup: 'popup-confirm',
            header: 'header-confirm',
            closeButton: 'close-button',
            icon: 'icon-confirm',
            content: 'content-confirm',
            cancelButton: 'cancel-button btn-round',
            confirmButton: 'confirm-button btn-round',
          }
        }).then((result) => {
          if (result) {
            modalBehavior.next(result);
          }
        });
        break;
      }
      case 'exito': {
        Swal.fire({
          title: titulo,
          text: texto,
          icon: 'success',
          showCancelButton,
          showConfirmButton: showConfirmButton,
          confirmButtonColor: '#E81F76',
          cancelButtonColor: '#ffffff',
          cancelButtonText: btnCerrar,
          confirmButtonText: btntext,
          allowOutsideClick: false,
          allowEscapeKey: false,
          reverseButtons: true,
          html: textHtml,
          customClass: {
            popup: 'popup-confirm',
            header: 'header-confirm',
            closeButton: 'close-button',
            icon: 'icon-confirm',
            content: 'content-confirm',
            cancelButton: 'cancel-button btn-round',
            confirmButton: 'confirm-button btn-round',
          }
        }).then((result) => {
          if (result) {
            modalBehavior.next(result);
          }
        });
        break;
      }
      case 'error': {
        Swal.fire({
          title: titulo,
          text: texto,
          icon: 'error',
          showCancelButton: true,
          confirmButtonColor: '#E81F76',
          cancelButtonColor: '#ffffff',
          confirmButtonText: '',
          cancelButtonText: btnCerrar,
          reverseButtons: true,
          allowOutsideClick: false,
          allowEscapeKey: false,
          showConfirmButton,
          html: textHtml,
          customClass: {
            popup: 'popup-error',
            header: 'header-error',
            closeButton: 'close-button',
            icon: 'icon-error',
            content: 'content-error',
          }
        })
        .then((result) => {
          if (result.value) {
            modalBehavior.next(result);
          }
        });
        break;
      }
      case 'warning': {
        Swal.fire({
          title: titulo,
          text: texto,
          icon: 'warning',
          showCancelButton,
          showConfirmButton: showConfirmButton,
          confirmButtonColor: '#E81F76',
          cancelButtonColor: '#ffffff',
          cancelButtonText: btnCerrar,
          confirmButtonText: btntext,
          allowOutsideClick: false,
          allowEscapeKey: false,
          reverseButtons: true,
          html: textHtml,
          customClass: {
            popup: 'popup-confirm',
            header: 'header-confirm',
            closeButton: 'close-button',
            icon: 'icon-confirm',
            content: 'content-confirm',
            cancelButton: 'cancel-button btn-round',
            confirmButton: 'confirm-button btn-round',
          }
        }).then((result) => {
          if (result.value) {
            modalBehavior.next(result);
          }
        });
        break;
      }
      case 'displayInfo': {
        Swal.fire({
          title: titulo,
          text: texto,
          showCancelButton: false,
          showConfirmButton,
          confirmButtonColor: '#E81F76',
          cancelButtonColor: '#ffffff',
          cancelButtonText: 'Cancelar',
          confirmButtonText: btntext,
          allowOutsideClick: false,
          allowEscapeKey: false,
          width: 750,
          html: textHtml,
          focusConfirm: false,
          customClass: {
            popup: 'popup-info',
            header: 'header-info',
            title: 'subtitle-govco',
            closeButton: 'close-button',
            content: 'content-info',
            confirmButton: 'confirm-button btn-round',
          }
        });
        break;
      }
      default: {
         // statements;
         break;
      }
    }
    return $modalBehavior;
  }
  mensajeConfirmar({
    titulo_confirmar,
    texto_confirmar,
    btntext_confirmar = 'Aceptar',
    showConfirmButton = true
  }) {
    const $modalBehavior = from(
      Swal.fire({
        title: titulo_confirmar,
        text: texto_confirmar,
        icon: 'success',
        showCloseButton: false,
        showConfirmButton,
        allowOutsideClick: false,
        allowEscapeKey: false,
        confirmButtonText: btntext_confirmar,
        customClass: {
          popup: 'popup-confirm',
          confirmButton: 'confirm-button btn-round',
        }
      })
    );
    return $modalBehavior;
  }
}
