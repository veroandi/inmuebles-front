import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { throwError, Observable, of, concat } from 'rxjs';
import { catchError, retryWhen, concatMap, delay, take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { MensajesModalService } from '../mensajes-modal/mensajes-modal.service';
import { LoginService } from 'src/app/modules/autenticacion/services/login.service';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  API_URL = environment.url_api;

  constructor(private http: HttpClient,
    private mensajeModal: MensajesModalService,
    private login: LoginService) { }

  public get<T>(url: string, params?: HttpParams): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get<T>(`${this.API_URL}${url}`, { headers, params }) // se comenta el '/' de apir url y url
      .pipe(
        retryWhen(errors => // Tolerancia de errores
          errors.pipe(
            concatMap(result => {
              if (result.status === 504) {
                return of(result);
              }
              return throwError(result);
            }),
            delay(1000),
            take(4),
            o => concat(o, throwError(`No fue posible conectarse con el servidor.`))
          )
        ),
        catchError((err: HttpErrorResponse) => {
          return this.handleError(err);
        })
      );
  }

  public post<T>(url: string, model: any, params?: HttpParams): Observable<any> {

    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.post<T>(`${this.API_URL}${url}`, model, { headers, params })
      .pipe(
        retryWhen(errors => // Tolerancia de errores
          errors.pipe(
            concatMap(result => {
              if (result.status === 504) {
                return of(result);
              }
              return throwError(result);
            }),
            delay(1000),
            take(4),
            o => concat(o, throwError(`No fue posible conectarse con el servidor.`))
          )
        ),
        catchError((err: HttpErrorResponse) => {
          return this.handleError(err);
        })
      );
  }

  public delete<T>(url: string, model: any, params?: HttpParams): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: model
    };

    return this.http.delete<T>(`${this.API_URL}${url}`, httpOptions)
      .pipe(
        retryWhen(errors => // Tolerancia de errores
          errors.pipe(
            concatMap(result => {
              if (result.status === 504) {
                return of(result);
              }
              return throwError(result);
            }),
            delay(1000),
            take(4),
            o => concat(o, throwError(`No fue posible conectarse con el servidor.`))
          )
        ),
        catchError((err: HttpErrorResponse) => {
          return this.handleError(err);
        })
      );
  }

  handleError(error: HttpErrorResponse) {
    let messageError: string;
    if (error.status == 404) {
      messageError = `<p>${error.error.message}</p>`;
      this.mensajeModal.abrirMensajeModal({
        tipo: 'error',
        titulo: 'Error',
        textHtml: messageError,
        btnCerrar: 'Cerrar',
        showConfirmButton: false
      });
    }
    else if (error.status == 403 || error.status == 401) {
      this.login.loggout();
      messageError = 'Indicando el siguiente error: </p><p><b>' + 'No autorizado' + '</b></p>';
      // log de errores
      this.mensajeModal.abrirMensajeModal({
        tipo: 'error',
        titulo: 'Proceso Fallido',
        textHtml: '<p>Se ha presentado un inconveniente y no es posible continuar con la operación. Por favor intente de nuevo, si el problema persiste comuníquese con la mesa de ayuda.',
        btnCerrar: 'Cerrar',
        showConfirmButton: false
      });
    }
    else {
      messageError = 'Indicando el siguiente error: </p><p><b>' + error.message + '</b></p>';
      // log de errores
      this.mensajeModal.abrirMensajeModal({
        tipo: 'error',
        titulo: 'Proceso Fallido',
        textHtml: '<p>Se ha presentado un inconveniente y no es posible continuar con la operación. Por favor intente de nuevo, si el problema persiste comuníquese con la mesa de ayuda.',
        btnCerrar: 'Cerrar',
        showConfirmButton: false
      });
    }
    return throwError(error);
  }
}
