import { Injectable } from '@angular/core';
import { FormControl} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidadorFormService {

  constructor() { }

    getValidacionesFormulario(fields: any[]): any {
    const controls = {};
    fields.forEach(res => {
      controls[res.formControlName] = new FormControl(res.defaultValue, res.validations, res.asyncValidations );
    });
    return controls;
  }
}
