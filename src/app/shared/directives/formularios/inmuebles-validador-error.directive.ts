import { Directive, ElementRef, HostListener, Renderer2, OnChanges, Input, SimpleChanges } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[inmueblesValidadorError]',
  providers: []
})
export class InmueblesValidadorErrorDirective implements OnChanges {

  @Input() formInvalid: any;

  constructor(private el: ElementRef,
              private renderer: Renderer2,
              private formControlName: NgControl) {}


  ngOnChanges(changes: SimpleChanges): void {
    this.onChange();
  }
  @HostListener('blur')
  onFocus() {
    this.onChange();
  }
  @HostListener('ngModelChange')
  async onChange() {
    const previosSibling = this.el.nativeElement.previousSibling;
    //const formControl = await this.formControlName.control;

    if (this.formControlName.invalid && this.formControlName.touched ) {
      if (previosSibling.tagName === 'LABEL') {
        this.renderer.addClass(previosSibling, 'label-error');
      }
    } else {
      if (previosSibling.tagName === 'LABEL') {
        this.renderer.removeClass(previosSibling, 'label-error');
      }
    }
  }
}
