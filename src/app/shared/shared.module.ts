import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedRoutingModule } from './shared-routing.module';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { InmueblesSelectorReactivoComponent } from './components/inmuebles-selector-reactivo/inmuebles-selector-reactivo.component';
import { InmueblesMensajesFormularioComponent } from './components/inmuebles-mensajes-formulario/inmuebles-mensajes-formulario.component';
import { HttpClientModule } from '@angular/common/http';
import { InmueblesValidadorErrorDirective } from './directives/formularios/inmuebles-validador-error.directive';

@NgModule({
  declarations: [
    InmueblesSelectorReactivoComponent,
    InmueblesMensajesFormularioComponent,
    InmueblesValidadorErrorDirective

  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule
  ],
  exports: [
    InmueblesSelectorReactivoComponent,
    InmueblesMensajesFormularioComponent,
    InmueblesValidadorErrorDirective

  ],
  providers: [ HttpClientModule]
})

export class SharedModule { }
