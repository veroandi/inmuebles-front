import { Injectable } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/general/general.service';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  constructor(private general: GeneralService) { }

  getAllProperty() {
    return this.general.get(environment.serviceUrls.property);
  }

  addProperty(property) {
    return this.general.post(environment.serviceUrls.property, property);
  }

  removeProperty(property) {
    return this.general.delete(environment.serviceUrls.property, property);
  }
}
