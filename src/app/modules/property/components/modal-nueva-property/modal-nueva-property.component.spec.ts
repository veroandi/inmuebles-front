/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalNuevaPropertyComponent } from './modal-nueva-property.component';

describe('ModalNuevaPropertyComponent', () => {
  let component: ModalNuevaPropertyComponent;
  let fixture: ComponentFixture<ModalNuevaPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalNuevaPropertyComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalNuevaPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
