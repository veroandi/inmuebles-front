import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { patronMensaje, patronValidacion } from 'src/app/shared/enum/validacion-formularios';
import { MensajesModalService } from 'src/app/shared/services/mensajes-modal/mensajes-modal.service';
import { ValidadorFormService } from 'src/app/shared/services/utils/validador-form.service';
import { PropertyService } from '../../services/property.service';

@Component({
  selector: 'inmuebles-modal-nueva-property',
  templateUrl: './modal-nueva-property.component.html',
  styleUrls: ['./modal-nueva-property.component.scss']
})
export class ModalNuevaPropertyComponent implements OnInit {

  @Input() data;

  public propertyForm: FormGroup;
  public camposForm: any[];
  fuente: string;
  modal1: HTMLCollectionOf<Element>;

  constructor(
    public activeModal: NgbActiveModal,
    private validadorFormService: ValidadorFormService,
    private fb: FormBuilder,
    private propertyService: PropertyService,
    private mensajeModal: MensajesModalService,
    private router: Router,) {}

  ngOnInit() {
    this.createForm();
    this.fuente = localStorage.getItem("tamanoFuente")
    this.modal1 = document.getElementsByClassName("modal-content")
  }

  public createForm() {
    this.camposForm = this.formValidations();
    this.propertyForm = this.fb.group(this.validadorFormService.getValidacionesFormulario(this.camposForm));
  }

  private formValidations(): any[] {
    let controles = [
      {
        formControlName: 'p_code',
        defaultValue: '',
        validations: [
          Validators.required,
          Validators.pattern(patronValidacion.alfanumerico),
        ],
        messages: [
          { error: 'required', message: patronMensaje.requerido },
          { error: 'pattern', message: patronMensaje.alfanumerico },
        ]
      },
      {
        formControlName: 'p_codeSAP',
        defaultValue: '',
        validations: [
          Validators.required,
          Validators.pattern(patronValidacion.alfanumerico),
        ],
        messages: [
          { error: 'required', message: patronMensaje.requerido },
          { error: 'pattern', message: patronMensaje.alfanumerico },
        ]
      },
      {
        formControlName: 'p_name',
        defaultValue: '',
        validations: [
          Validators.required,
          Validators.pattern(patronValidacion.alfanumerico),
        ],
        messages: [
          { error: 'required', message: patronMensaje.requerido },
          { error: 'pattern', message: patronMensaje.alfanumerico },
        ]
      },
      {
        formControlName: 'p_type',
        defaultValue: '',
        validations: [
          Validators.required,
          Validators.pattern(patronValidacion.alfanumerico),
        ],
        messages: [
          { error: 'required', message: patronMensaje.requerido },
          { error: 'pattern', message: patronMensaje.alfanumerico },
        ]
      },
      {
        formControlName: 'p_phonne',
        defaultValue: '',
        validations: [
          Validators.required,
          Validators.pattern(patronValidacion.soloNumeros),
        ],
        messages: [
          { error: 'required', message: patronMensaje.requerido },
          { error: 'pattern', message: patronMensaje.soloNumeros },
        ]
      },
      {
        formControlName: 'p_streetAddress',
        defaultValue: '',
        validations: [
          Validators.required,
          Validators.pattern(patronValidacion.alfanumericoEspacio),
        ],
        messages: [
          { error: 'required', message: patronMensaje.requerido },
          { error: 'pattern', message: patronMensaje.alfanumericoEspacio},
        ]
      },
      {
        formControlName: 'p_constructionYear',
        defaultValue: '',
        validations: [
          Validators.required,
          Validators.pattern(patronValidacion.alfanumerico),
        ],
        messages: [
          { error: 'required', message: patronMensaje.requerido },
          { error: 'pattern', message: patronMensaje.alfanumerico },
        ]
      },
      {
        formControlName: 'p_area',
        defaultValue: '',
        validations: [
          Validators.required,
          Validators.pattern(patronValidacion.soloNumeros),
        ],
        messages: [
          { error: 'required', message: patronMensaje.requerido },
          { error: 'pattern', message: patronMensaje.soloNumeros },
        ]
      },
      {
        formControlName: 'p_useFulLife',
        defaultValue: '',
        validations: [
          Validators.required,
          Validators.pattern(patronValidacion.soloNumeros),
        ],
        messages: [
          { error: 'required', message: patronMensaje.requerido },
          { error: 'pattern', message: patronMensaje.soloNumeros },
        ]
      },
    ];
    return controles;
  }

  public submit(): void {
    if (this.propertyForm.valid) {
      this.mensajeModal.abrirMensajeModal({
        tipo: 'confirm',
        titulo: '¿Está seguro de agregar el registro?',
        texto: '',
        btntext: 'ACEPTAR'
      }).subscribe((content: any) =>{
        if(content.value) {
          this.propertyService.addProperty(
            {
              id: null,
              code: this.propertyForm.controls.p_code.value === "" ? null : this.propertyForm.controls.p_code.value,
              codeSAP: this.propertyForm.controls.p_codeSAP.value === "" ? null : this.propertyForm.controls.p_codeSAP.value,
              name: this.propertyForm.controls.p_name.value === "" ? null : this.propertyForm.controls.p_name.value,
              type: this.propertyForm.controls.p_type.value === "" ? null : this.propertyForm.controls.p_type.value,
              phonne: this.propertyForm.controls.p_phonne.value === "" ? null : this.propertyForm.controls.p_phonne.value,
              streetAddress: this.propertyForm.controls.p_streetAddress.value === "" ? null : this.propertyForm.controls.p_streetAddress.value,
              constructionYear: this.propertyForm.controls.p_constructionYear.value === "" ? null : this.propertyForm.controls.p_constructionYear.value,
              area: this.propertyForm.controls.p_area.value === "" ? null : this.propertyForm.controls.p_area.value,
              useFulLife: this.propertyForm.controls.p_useFulLife.value === "" ? null : this.propertyForm.controls.p_useFulLife.value,
            }

          ).subscribe(data => {
            this.mensajeModal.mensajeConfirmar({
              titulo_confirmar: 'Registro agregado satisfactoriamente',
              texto_confirmar: '',
              btntext_confirmar: 'ACEPTAR'
            }).subscribe((content: any) =>{
              if(content.value) {
                location.reload();
              }
            });
          },
            error => {
              // Los errores y mensajes son controlados en el general service
            }
          );
        }
      });
    }

    if (this.propertyForm.invalid) {
      return Object.values(this.propertyForm.controls).forEach(control => {
        control.markAllAsTouched();
      });
    }
  }

  ngAfterViewInit() {
    this.setFontsize();
  }

  setFontsize() {
    this.fuente = localStorage.getItem("tamanoFuente")
    let listadoSpan = this.modal1[0].getElementsByTagName("span");
    for (var i = 0; i < listadoSpan.length; i++) {
      let actual = listadoSpan[i];
      actual.id = this.fuente;
    }
  }
}
