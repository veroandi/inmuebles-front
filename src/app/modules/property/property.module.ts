import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PropertyRoutingModule } from './property-routing.module';
import { PropertyComponent } from './property.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {DataTablesModule} from 'angular-datatables';
import { ModalNuevaPropertyComponent } from './components/modal-nueva-property/modal-nueva-property.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [PropertyComponent, ModalNuevaPropertyComponent],
  imports: [
    CommonModule,
    PropertyRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    NgbModalModule
  ], entryComponents: [ModalNuevaPropertyComponent],
})
export class PropertyModule { }
