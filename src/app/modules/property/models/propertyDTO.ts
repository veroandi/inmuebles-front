export interface propertyDTO {
  id: number;
  code: string;
  codeSAP: string;
  name: string;
  type: string;
  phonne: number;
  streetAddress: string;
  constructionYear: string;
  area: number;
  useFulLife: number;
}
