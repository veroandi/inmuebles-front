import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { LoginService } from '../autenticacion/services/login.service';
import { HeaderService } from 'src/app/core/components/header/services/header.service';
import { PropertyService } from './services/property.service';
import { propertyDTO } from './models/propertyDTO';
import { MensajesModalService } from 'src/app/shared/services/mensajes-modal/mensajes-modal.service';
import { DataTableDirective } from 'angular-datatables';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalNuevaPropertyComponent } from './components/modal-nueva-property/modal-nueva-property.component';

@Component({
  selector: 'inmuebles-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.scss']
})
export class PropertyComponent implements OnInit, OnDestroy {

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;

  usuario$: Observable<any>;
  public listProperty: propertyDTO[];
  public dtOptions: DataTables.Settings = {language: {
    processing: "Procesando...",
    search: "Buscar:",
    lengthMenu: "Mostrar _MENU_ elementos",
    info: "Mostrando desde _START_ al _END_ de _TOTAL_ elementos",
    infoEmpty: "Mostrando ningún elemento.",
    infoFiltered: "(filtrado _MAX_ elementos total)",
    infoPostFix: "",
    loadingRecords: "Cargando registros...",
    zeroRecords: "No se encontraron registros",
    emptyTable: "No hay datos disponibles en la tabla",
    paginate: {
      first: "Primero",
      previous: "Anterior",
      next: "Siguiente",
      last: "Último"
    },
    aria: {
      sortAscending: ": Activar para ordenar la tabla en orden ascendente",
      sortDescending: ": Activar para ordenar la tabla en orden descendente"
    }
  }};
  public dtTrigger: Subject<any> = new Subject<any>();

  constructor(public auth: LoginService,
    public headerService: HeaderService,
    private propertyService: PropertyService,
    private mensajeModal: MensajesModalService,
    private modalService: NgbModal) {}

  ngOnInit(): void {
    this.reloadUsuario();
    this.headerService.asignarInformacionEspecifica('CRUD PROPERTY');
    this.cargarProperty(true);
  }
  ngAfterViewInit() {
    this.setFontsize();
  }

  setFontsize() {
    let fuente = localStorage.getItem("tamanoFuente")
    let listadoSpan = document.getElementsByTagName("span");
    for (var i = 0; i < listadoSpan.length; i++) {
      let actual = listadoSpan[i];
        actual.id = fuente;
    }
  }

  cargarProperty(primeraVez) {
    this.propertyService.getAllProperty().subscribe(
      result => {
        this.listProperty = result;
        if(primeraVez) {
          this.dtTrigger.next(null);
        }
      }, (error) => {
      });
  }

  abrirModal(){
    this.modalService.open(ModalNuevaPropertyComponent, { backdrop: 'static', centered: true });
  }

  removeProperty(property) {
    this.mensajeModal.abrirMensajeModal({
      tipo: 'confirm',
      titulo: '¿Está seguro de eliminar el registro?',
      texto: '',
      btntext: 'ACEPTAR'
    }).subscribe((content: any) =>{
      if(content.value) {
        this.propertyService.removeProperty(property).subscribe(
          result => {
            this.mensajeModal.mensajeConfirmar({
              titulo_confirmar: 'Registro eliminado satisfactoriamente',
              texto_confirmar: '',
              btntext_confirmar: 'ACEPTAR'
            }).subscribe((content: any) => {
              this.rerender();
              this.cargarProperty(false);
            }) ;
          }, (error) => {});
      }
    });
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
    dtInstance.destroy();
    this.dtTrigger.next(null);
      });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  reloadUsuario() {
    this.usuario$ = this.auth.usuario$
  }

}
