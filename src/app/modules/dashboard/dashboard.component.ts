import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'inmuebles-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  public esMovil: boolean;

  constructor() { }

  ngOnInit(): void {
    this.getMovil();
  }

  ngAfterViewInit() {
    this.setFontsize();
  }

  setFontsize() {
    let fuente = localStorage.getItem("tamanoFuente")
    let listadoSpan = document.getElementsByTagName("span");
    let listadoP = document.getElementsByTagName("p");
    for (var i = 0; i < listadoP.length; i++) {
      let actual = listadoP[i];
        actual.id = fuente;
    }

    for (var i = 0; i < listadoSpan.length; i++) {
      let actual = listadoSpan[i];
      actual.id = fuente;
    }
  }
  contraste(): void {
    var elem = document.getElementById('html');
    elem.classList.add('altoContraste');
  }

  getMovil() {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      this.esMovil = true;

    } else {
      this.esMovil = false;
    }
  }
}
