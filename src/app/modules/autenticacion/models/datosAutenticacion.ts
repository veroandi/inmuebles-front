export interface DatosAutenticacion {
  user: string;
  pwd: string;
  token: string;
}
