import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, throwError, concat } from 'rxjs';
import { map, retryWhen, concatMap, delay, take, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient, HttpErrorResponse } from '@angular/common/http';
import { MensajesModalService } from 'src/app/shared/services/mensajes-modal/mensajes-modal.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  //Este servicio actúa como store en el cliente conservando el perfil del usuario.
  //Para soportar el refresh del browser se guarda la data en el localStorage.

  private subject = new BehaviorSubject<any>(null);

  // este observable guarda el perfil del usuario y puede ser usado en otros componentes ver ejemplo en el componente header
  usuario$: Observable<any> = this.subject.asObservable();
  //
  isLoggedIn$: Observable<boolean>;
  isLoggedOut$: Observable<boolean>;

  constructor(
    private router: Router,
    private http: HttpClient,
    private mensajeModal: MensajesModalService) {

    this.isLoggedIn$ = this.usuario$.pipe(map(user => !!user));
    this.isLoggedOut$ = this.isLoggedIn$.pipe(map(loggedIn => !loggedIn));

    const user = localStorage.getItem('user');
    if (user !== '' && user !== null && user !== undefined && user !== "undefined") {
      this.subject.next(JSON.parse(user));
    }
  }

  login(user) {

    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.post<any>(`${environment.url_api}${environment.serviceUrls.login}`, user, { headers })
      .pipe(
        retryWhen(errors => // Tolerancia de errores
          errors.pipe(
            concatMap(result => {
              if (result.status === 504) {
                return of(result);
              }
              return throwError(result);
            }),
            delay(1000),
            take(4),
            o => concat(o, throwError(`No fue posible conectarse con el servidor.`))
          )
        ),
        catchError((err: HttpErrorResponse) => {
          return this.handleError(err);
        })
      );
  }

  public updateObservable(user) {
    localStorage.removeItem('user');
    this.subject.next(user);
    localStorage.setItem('user', JSON.stringify(user));
  }

  public loggout() {
    localStorage.removeItem('dataAutenticacion');
    localStorage.removeItem('tokenAutenticacion');
    localStorage.removeItem('user');
    localStorage.clear();
    this.router.navigate(['login']);
    this.subject.next(null);
    var elem = document.getElementById('html');
    elem.classList.remove('altoContraste');
  }

  public getTokenAutenticado() {
    return localStorage.getItem('tokenAutenticacion');
  }

  public getUsuario() {
    let usuario
    this.usuario$.subscribe(
      user => { usuario = user },
      err => { console.log(err) }
    );
    return usuario
  }

  private handleError(error: HttpErrorResponse) {
    let messageError: string;
    if (error.status == 404) {
      messageError = `<p>${error.error.message}</p>`;
      this.mensajeModal.abrirMensajeModal({
        tipo: 'error',
        titulo: 'Error',
        textHtml: messageError,
        btnCerrar: 'Cerrar',
        showConfirmButton: false
      });
    }
    else if (error.status == 403 || error.status == 401) {
      this.loggout();
      messageError = 'Indicando el siguiente error: </p><p><b>' + 'No autorizado' + '</b></p>';
      // log de errores
      this.mensajeModal.abrirMensajeModal({
        tipo: 'error',
        titulo: 'Proceso Fallido',
        textHtml: '<p>Se ha presentado un inconveniente y no es posible continuar con la operación. Por favor intente de nuevo, si el problema persiste comuníquese con la mesa de ayuda.',
        btnCerrar: 'Cerrar',
        showConfirmButton: false
      });
    }
    else {
      messageError = 'Indicando el siguiente error: </p><p><b>' + error.message + '</b></p>';
      // log de errores
      this.mensajeModal.abrirMensajeModal({
        tipo: 'error',
        titulo: 'Proceso Fallido',
        textHtml: '<p>Se ha presentado un inconveniente y no es posible continuar con la operación. Por favor intente de nuevo, si el problema persiste comuníquese con la mesa de ayuda.',
        btnCerrar: 'Cerrar',
        showConfirmButton: false
      });
    }
    return throwError(error);
  }
}
