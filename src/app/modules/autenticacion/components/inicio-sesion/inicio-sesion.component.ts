import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ValidadorFormService } from 'src/app/shared/services/utils/validador-form.service';
import { environment } from 'src/environments/environment'
import { LoginService } from './../../services/login.service';
import { Router } from '@angular/router';
import { MensajesModalService } from 'src/app/shared/services/mensajes-modal/mensajes-modal.service';
import { DatePipe } from '@angular/common';
import { DatosAutenticacion } from '../../models/datosAutenticacion';
import { patronValidacion, patronMensaje } from 'src/app/shared/enum/validacion-formularios';

@Component({
  selector: 'inmuebles-inicio-sesion',
  templateUrl: './inicio-sesion.component.html',
  styleUrls: ['./inicio-sesion.component.scss'],
  providers: [DatePipe]
})

export class InicioSesionComponent implements OnInit {

  public loginForm: FormGroup;
  public camposForm: any[];
  // variables de autenticacion
  public dataAutenticacion: DatosAutenticacion;
  //----------RECAPTCHA--------------
  public SITE_KEY;//llave del recaptcha
  public CAPTCHA: number;//visibilidad del recaptcha
  //-----------------------------------------
  @Output() messageEvent: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private validadorFormService: ValidadorFormService,
    private loginService: LoginService,
    private mensajeModal: MensajesModalService,
  ) {
    this.SITE_KEY = environment.captcha.key;
    this.CAPTCHA = environment.captcha.estado;
  }

  ngOnInit(): void {
    this.createForm();
  }

  ngAfterViewInit(): void {
    this.verificarParametroRecaptcha();
  }

  public createForm() {
    this.camposForm = this.formValidations();
    this.loginForm = this.fb.group(this.validadorFormService.getValidacionesFormulario(this.camposForm));
  }

  private formValidations(): any[] {
    let controles = [
      {
        formControlName: 'usuario',
        defaultValue: '',
        validations: [Validators.required],
        messages: [
          { error: 'required', message: patronMensaje.requerido }
        ]
      },
      {
        formControlName: 'contrasenia',
        defaultValue: '',
        validations: [Validators.required],
        messages: [
          { error: 'required', message: patronMensaje.requerido }
        ]
      },
      {
        formControlName: 'codigoAcceso',
        defaultValue: '',
        validations: [
          Validators.required,
          Validators.pattern(patronValidacion.soloNumeros),
          Validators.minLength(4),
          Validators.maxLength(10)
        ],
        messages: [
          { error: 'required', message: patronMensaje.requerido },
          { error: 'pattern', message: patronMensaje.soloNumeros },
          { error: 'minlength', message: 'Mínimo de 4 caracteres' },
          { error: 'maxlength', message: 'Máximo 10 caracteres' }
        ]
      },
      {
        formControlName: 'recaptcha',
        defaultValue: null,
        validations: [Validators.required],
        messages: [
          { error: 'required', message: patronMensaje.requerido },
        ]
      },
    ];
    //Si el parametro del recaptcha está inactivo remover el validator required del control recaptcha
    return controles;
  }

  public submit(): void {
    if ((this.loginForm.valid == false)) {
      this.mensajeModal.abrirMensajeModal({
        tipo: 'warning',
        titulo: 'Advertencia',
        textHtml: '<p>Debe diligenciar los campos del formulario.</p>',
        btnCerrar: 'Cerrar',
        showConfirmButton: false
      });
      return;
    }

    if (this.loginForm.valid) {
      this.loginService.login(
        {
          user: this.valorUsuarioFormulario,
          pwd: this.valorContraseniaFormulario,
          code: this.valorCodigoAccesoFormulario,
        }

      ).subscribe(data => {

        this.dataAutenticacion = data;
        localStorage.setItem('dataAutenticacion', JSON.stringify(this.dataAutenticacion));
        localStorage.setItem('tokenAutenticacion', data.token);
        this.loginService.updateObservable(this.dataAutenticacion);
        this.router.navigate(['../']);
      },
        error => {
          // Los errores y mensajes son controlados en el general service
        }
      );
    }

    if (this.loginForm.invalid) {
      return Object.values(this.loginForm.controls).forEach(control => {
        control.markAllAsTouched();
      });
    }

  }

  //obtener el valor de los input
  get valorUsuarioFormulario() {
    return this.loginForm.controls.usuario.value === "" ? null : this.loginForm.controls.usuario.value;
  }

  get valorContraseniaFormulario() {
    return this.loginForm.controls.contrasenia.value === "" ? null : this.loginForm.controls.contrasenia.value;
  }

  get valorCodigoAccesoFormulario() {
    return this.loginForm.controls.codigoAcceso.value === "" ? null : this.loginForm.controls.codigoAcceso.value;
  }

  verificarParametroRecaptcha() {
    if (this.CAPTCHA != 1) {
      this.loginForm.controls.recaptcha.clearValidators();
      this.loginForm.controls.recaptcha.updateValueAndValidity();
    }
  }
}
