import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateComponent } from './components/template/template.component';
import { HeaderComponent } from './components/header/header.component';
import { ContentComponent } from './components/content/content.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { AccessibilityComponent } from './components/accessibility/accessibility.component';
import { HttpClientModule } from '@angular/common/http';
import { AutenticacionModule } from '../modules/autenticacion/autenticacion.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NavBarLeftComponent } from './components/navbar-left/navbar-left.component';



@NgModule({
  declarations: [
    TemplateComponent,
    HeaderComponent,
    ContentComponent,
    FooterComponent,
    AccessibilityComponent,
    NavbarComponent,
    NavBarLeftComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    AutenticacionModule
  ],
  exports: [
    TemplateComponent,
  ],
  providers: [HttpClientModule]
})
export class CoreModule { }
