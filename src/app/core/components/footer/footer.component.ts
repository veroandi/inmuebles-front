import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/modules/autenticacion/services/login.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'inmuebles-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  usuario$:Observable<any>;
  public config:any;

  constructor(public auth:LoginService) {
    this.config = [];
  }

  ngOnInit(): void {
    this.reloadUsuario();
  }
  reloadUsuario(){
    this.usuario$=this.auth.usuario$
  }
}
