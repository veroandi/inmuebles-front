import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoginService } from 'src/app/modules/autenticacion/services/login.service';
import { interval, Observable, Subscription } from 'rxjs';
import { HeaderService } from 'src/app/core/components/header/services/header.service';
import { NavigationEnd, NavigationError, NavigationStart, Router, Event } from '@angular/router';
import { take } from 'rxjs/operators';

@Component({
  selector: 'inmuebles-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  usuario$: Observable<any>;
  public config: any = { titulo: ''};
  destruirServicio: Subscription;

  constructor(public auth: LoginService, public headerService: HeaderService, private router: Router) {    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        if (event.url === '/') {
              this.config.titulo = "INMUEBLES";
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.destruirServicio.unsubscribe();
  }

  ngOnInit(): void {
    this.reloadUsuario();
    this.destruirServicio = this.headerService.textoHeader$.subscribe(res => {
      interval(100).pipe(take(1)).subscribe(resp => {
        this.config.titulo = res.titulo;
      })
    });
  }

  reloadUsuario() {
    this.usuario$ = this.auth.usuario$
  }
}
