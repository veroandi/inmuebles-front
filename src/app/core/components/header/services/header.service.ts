import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
  })
  export class HeaderService {

    private subject = new Subject<any>();
    textoHeader$:Observable<any> = this.subject.asObservable();


    public asignarInformacionEspecifica(titulo: string, cuerpo?: string){
        this.subject.next({titulo, cuerpo});  
    }

  }