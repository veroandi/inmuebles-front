import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavBarLeftComponent } from './navbar-left.component';

describe('NavBarLeftComponent', () => {
  let component: NavBarLeftComponent;
  let fixture: ComponentFixture<NavBarLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavBarLeftComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavBarLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
