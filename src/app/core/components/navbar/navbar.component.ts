import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/modules/autenticacion/services/login.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'inmuebles-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  usuario$: Observable<any>;
  examenNav: string;
  constructor(
    public auth: LoginService,
  ) {
  }

  ngOnInit(): void {
    this.reloadUsuario();

  }

  reloadUsuario() {
    this.usuario$ = this.auth.usuario$
  }

  logout() {
    this.auth.loggout();
  }

}
