import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/modules/autenticacion/services/login.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'inmuebles-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  usuario$:Observable<any>;

  constructor(public auth: LoginService) { }

  ngOnInit(): void {
    this.reloadUsuario();
  }

  reloadUsuario(){
    this.usuario$=this.auth.usuario$
  }

  onActivate(e) {
    window.scroll(0, 0);
  }
}
