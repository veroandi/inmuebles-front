import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'inmuebles-accessibility',
  templateUrl: './accessibility.component.html',
  styleUrls: ['./accessibility.component.scss']
})
export class AccessibilityComponent implements OnInit {

  contrasteActivo: boolean;
  tamanoLetra: number;
  initialSize: string;
  listadoP: any;
  listadoSpan: any;
  listadoLabel: any;
  si: number;
  cuentaAumento: number;


  constructor() { }

  ngOnInit(): void {
    this.contrasteActivo = false;
    this.listadoP = document.getElementsByTagName("p");
    this.listadoSpan = document.getElementsByTagName("span");
    if (localStorage.getItem("tamanoFuente") == null) {
      this.cuentaAumento = 0;
    } else {
      if (localStorage.getItem("tamanoFuente") == 'fuente0' || localStorage.getItem("tamanoFuente") == 'fuente1') {
        this.cuentaAumento = 2;
      } else if (localStorage.getItem("tamanoFuente") == 'fuente2' || localStorage.getItem("tamanoFuente") == 'fuente3') {
        this.cuentaAumento = 4;
      } else if (localStorage.getItem("tamanoFuente") == 'fuente4' || localStorage.getItem("tamanoFuente") == 'fuente5') {
        this.cuentaAumento = 6;
      } else if (localStorage.getItem("tamanoFuente") == 'fuente6' || localStorage.getItem("tamanoFuente") == 'fuente7') {
        this.cuentaAumento = 8;
      }
    }
  }

  mas(): void {
    if (this.cuentaAumento < 8) {
      let flag1 = true;
      let clase = "fuente"
      for (var i = 0; i < this.listadoP.length; i++) {
        let actual = this.listadoP[i];
        if (actual.firstChild != null) {
          actual.firstChild.className = "fuente-mas"
        }
        actual.id = (clase + this.cuentaAumento);
        if (this.cuentaAumento == 0 && flag1) {
          localStorage.setItem("tamanoFuente", clase + this.cuentaAumento)
        } else if (this.cuentaAumento == 2 && flag1) {
          localStorage.setItem("tamanoFuente", clase + this.cuentaAumento)
        } else if (this.cuentaAumento == 4 && flag1) {
          localStorage.setItem("tamanoFuente", clase + this.cuentaAumento)
        } else if (this.cuentaAumento == 6 && flag1) {
          localStorage.setItem("tamanoFuente", clase + this.cuentaAumento)
        }
        if (flag1) {
          this.cuentaAumento++
          flag1 = false;
        }
      }

      let flag2 = true;
      for (var i = 0; i < this.listadoSpan.length; i++) {
        let actual = this.listadoSpan[i];
        actual.id = (clase + this.cuentaAumento);
        if (this.cuentaAumento == 1 && flag2) {
          localStorage.setItem("tamanoFuente", clase + this.cuentaAumento)
        } else if (this.cuentaAumento == 3 && flag2) {
          localStorage.setItem("tamanoFuente", clase + this.cuentaAumento)
        } else if (this.cuentaAumento == 5 && flag2) {
          localStorage.setItem("tamanoFuente", clase + this.cuentaAumento)
        } else if (this.cuentaAumento == 7 && flag2) {
          localStorage.setItem("tamanoFuente", clase + this.cuentaAumento)
        }
        if (flag2) {
          this.cuentaAumento++
          flag2 = false;
        }
      }
    }


  }
  menos(): void {
    this.cuentaAumento = 0
    for (var i = 0; i < this.listadoP.length; i++) {
      let actual = this.listadoP[i];
      actual.id = "."
      actual.classList.remove('fuente-mas')
    }

    for (var i = 0; i < this.listadoSpan.length; i++) {
      let actual = this.listadoSpan[i];
      actual.id = "."
      actual.classList.remove('fuente-mas')
    }
    localStorage.setItem("tamanoFuente", '.')
  }

  contraste(): void {
    var elem = document.getElementById('html');
    if (localStorage.getItem('contraste') == 'S') {
      elem.classList.remove('altoContraste');
      localStorage.setItem('contraste', 'N')
    } else {
      elem.classList.add('altoContraste');
      localStorage.setItem('contraste', 'S')
    }
  }
}
