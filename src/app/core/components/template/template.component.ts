import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'inmuebles-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss']
})
export class TemplateComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  adentro(){
    return localStorage.getItem('dataAutenticacion')!=null
  }

}
