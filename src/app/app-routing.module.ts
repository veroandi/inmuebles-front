import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './helpers/auth.guard';
import { LoginGuard } from './helpers/login/login.guard';


const routes: Routes = [
  {
    path: '', canActivate: [AuthGuard], children: [

      {
        path: '',
        loadChildren: () => import('./modules/dashboard/dashboard.module').then(m => m.DashboardModule),
        data: { breadcrumb: 'Inicio' }
      },
      {
        path: 'property',
        loadChildren: () => import('./modules/property/property.module').then(m => m.PropertyModule),
        data: { breadcrumb: 'CRUD Property' }
      },
    ]
  },

  {
    path: 'login',
    canActivate: [LoginGuard],
    loadChildren: () => import('./modules/autenticacion/autenticacion.module').then(m => m.AutenticacionModule),
    data: { breadcrumb: '' }
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
