# base image
FROM node:10.13
# set working directory
RUN mkdir /usr/src/app
WORKDIR /usr/src/app
# add .bin to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH
# install package.json (o sea las dependencies)
COPY package.json /usr/src/app/package.json
RUN npm install --cache
RUN npm install -g @angular/cli@11.2.14
# add app
COPY . /usr/src/app
# start app
CMD ng serve --prod --host 0.0.0.0 --port 3001 --disable-host-check
